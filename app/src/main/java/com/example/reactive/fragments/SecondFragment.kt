package com.example.reactive.fragments

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.fragment.NavHostFragment
import com.example.reactive.GsonParser
import com.example.reactive.People
import com.example.reactive.R
import com.example.reactive.retrofit.APIInterface
import com.example.reactive.retrofit.PostsItem
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedReader
import java.io.File
import java.io.InputStream
import java.io.InputStreamReader
import java.io.UTFDataFormatException
import java.net.URL

const val BASE_URL = "https://jsonplaceholder.typicode.com/posts"

class SecondFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_second, container, false)

        val btnToFirstFragment: Button = fragmentLayout.findViewById(R.id.btnToFirstFrag)
        val btnRestData: Button = fragmentLayout.findViewById(R.id.btnRestData)
        val btnJsonData: Button = fragmentLayout.findViewById(R.id.btnJsonData)
        val tvData: TextView = fragmentLayout.findViewById(R.id.tvData)
        //navigate
        btnToFirstFragment.setOnClickListener {
            val navController = NavHostFragment.findNavController(this)
            navController.navigate(R.id.firstFragment)
        }
        //retrofit get
        btnRestData.setOnClickListener {
            val subsriber = getRestData()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    tvData.setText(it)
                    Log.i("rest", it)
                },{
                    tvData.setText(it.message)
                },{
                    tvData.setText("Data is over")
                }
                )
        }
        //json get
        btnJsonData.setOnClickListener {
            val subsriber = getJsonData()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    tvData.setText(it)
                },{
                    tvData.setText(it.message)
                },{
                    tvData.setText("Data is over")
                })
        }
        return fragmentLayout
    }

    companion object {

        @JvmStatic
        fun newInstance() =
            SecondFragment()
    }

    fun getJsonData(): Observable<String> {
        return Observable.create { subscriber ->
            val stream = activity?.assets?.open("people.json")
            val jsonFile= stream?.bufferedReader().let{ it?.readText()}
            val jsonArray = JSONObject(jsonFile).getJSONArray("people")
            var line = ""
            for (i in (0..jsonArray.length()-1)){
                line = jsonArray.getString(i)
                subscriber.onNext(line)
                Thread.sleep(500)
            }
            subscriber.onComplete()
        }
    }

    fun getRestData(): Observable<String> {
        return Observable.create { subscriber ->
            val url = URL(BASE_URL)
            val conn = url.openConnection()
            val br = BufferedReader(InputStreamReader(conn.getInputStream()))
            var line: String? = br.readLine()
            while(line!=null){
                subscriber.onNext(line)
                line = br.readLine()
                Thread.sleep(500)
            }
            subscriber.onComplete()
        }
    }

}