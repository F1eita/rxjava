package com.example.reactive.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.navigation.fragment.NavHostFragment
import com.example.reactive.R


class FirstFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout = inflater.inflate(R.layout.fragment_first, container, false)

        val btnToSecFragment: Button = fragmentLayout.findViewById(R.id.btnToSecFrag)

        btnToSecFragment.setOnClickListener {
            val navController = NavHostFragment.findNavController(this)
            navController.navigate(R.id.secondFragment)
        }

        return fragmentLayout
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            FirstFragment()
    }
}