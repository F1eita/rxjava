package com.example.reactive

class JRoot(var people: ArrayList<People>) {

    override fun toString(): String {
        return "Root{people = ${people}}"
    }
}