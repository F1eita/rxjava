package com.example.reactive.retrofit


data class PostsItem(
                val body: String,
                val id: Int,
                val title: String,
                val userId: Int){
        override fun toString(): String {
                return "UserId: $userId, Id: $id, title: $title"
        }
}

