package com.example.reactive.retrofit


import io.reactivex.Single
import retrofit2.http.GET

interface APIInterface {
    @GET("posts")
    fun getData(): Single<List<PostsItem>>

}