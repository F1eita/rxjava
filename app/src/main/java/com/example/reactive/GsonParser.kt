package com.example.reactive

import com.google.gson.Gson
import java.io.BufferedReader
import java.io.File
import java.io.FileReader
import java.io.InputStreamReader

class GsonParser {

    fun parse(): JRoot{
        val gson = Gson()
        val reader = FileReader("people.json")
        val root = gson.fromJson(reader, JRoot::class.java)
        return root
    }
}