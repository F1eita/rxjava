package com.example.reactive

data class People(val id: Int, val name: String) {
    override fun toString(): String {
        return "People{id: $id, name: $name}"
    }
}